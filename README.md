# Node-red server for Sofus Addingtons master thesis prototype

This repository includes project files for the server deployment, build with Node-red.

## Run
The deployment is build on [Docker](https://www.docker.com).
To run the application with Docker, run the start-docker script

```
sudo sh start-docker
```

This is only tested with MacOS Catalina.

The node-red interface should now be available at `http://localhost:1880/admin/`

The login is:
```
username: admin
password: sofus-thesis
```

## Web Dashboard
When the container is running, can the web dashboard be found at `http://localhost:1880/areas/`

The code for the dashboard can be found under [/src/web-app](https://gitlab.gbar.dtu.dk/s124141/sofus-thesis/tree/master/src/web-app)

## Scanner iOS Shortcut
The shortcut for the scanner can be found at [This link](https://www.icloud.com/shortcuts/05a13fc12c2b41768bedce0707fec159?fbclid=IwAR15vs8WlYZ0hFnSiAut8qhJGeamOi7c1JDUixq2OYdIrvn2t5fYq3nmnpk)