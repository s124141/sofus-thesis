
const URL = 'http://sofusa-36314.portmap.host:36314';

var request = new XMLHttpRequest()

request.open('GET', URL + "/getAreas", true)
request.onload = function () {
    var data = JSON.parse(this.response);

    if (request.status >= 200 && request.status < 400) {


        for (var i in data) {
            
            if (data[i].ID !== 0) {
                drawRow(i, data[i]);
            }
        }

    } else {
        console.log('error')

    }
}

request.send()

var style = getComputedStyle(document.body);
var colorArray = [
    "#AA00FF",
    "#6200EA",
    "#00B8D4",
    "#00BFA5",
    "#004D40",
    "#C0CA33",
    "#F57F17",
    "#795548",
    "#607D8B"
];
var usedColor = 0;

function drawRow(index, data) {
    

    data.dataset = [];
    data.start = 99999999999999;
    data.end = 0;
    var alarmCounter = 0;
    var alarmStatus = false;

    for (var i = 0; i < data.sources.length; i++) {

        // Format the data labels to fit chartjs
        for (var j in data.sources[i].data) {
            data.sources[i].data[j] = { t: data.sources[i].data[j].time * 1000, y: data.sources[i].data[j].value };
        }

        // Remove empty sources
        if (data.sources[i].data.length < 1) {
            data.sources.splice(i, 1);
            i = i - 1;
            continue;
        }


        // Set correct start and stop times
        if (data.sources[i].start < data.start) {
            data.start = data.sources[i].start;
        }
        if (data.sources[i].end === 0) {
            data.end = Date.now();
        } else if (data.sources[i].end > data.end) {
            data.end = data.sources[i].end;
        }


        // Push alarms
        if (data.sources[i].alarms.length >= 1) {
            for (var alarm in data.sources[i].alarms) {
                // If alarm starts before this timeline, push it to start
                if (data.sources[i].alarms[i].value_time < data.start) {
                    data.sources[i].alarms[i].value_time = data.start/1000
                }


                if (data.sources[i].alarms[alarm].open_end) {
                    alarmStatus = true;
                }

                data.dataset.push(
                    {
                        label: 'Alarm ' + data.sources[i].alarms[alarm].type,
                        data: [{ t: data.sources[i].alarms[alarm].value_time * 1000, y: data.sources[i].alarms[alarm].value }],
                        borderWidth: 5,
                        borderColor: "#ff0000",
                        //backgroundColor: '#007bff',
                        fill: false,
                        pointRadius: 2,
                        // hoverRadius: 0,
                    }
                );
                alarmCounter++;
            }
        }
        

        // Push each source to the data set
        data.dataset.push(
            {
                label: 'Sensor ' + data.sources[i].ID,
                data: data.sources[i].data,
                borderWidth: 3,
                //borderColor: colorArray[data.sources[i].area],
                borderColor: colorArray[usedColor],
                //backgroundColor: '#007bff',
                fill: false,
                pointRadius: 0,
                hoverRadius: 0,
                lineTension: 0.1
            }
        )
        usedColor++;
    }

    document.getElementById("chart" + index).innerHTML = drawMarkup(index, data.ID);

    document.getElementById("chartTitle" + index).innerHTML = 'Area: ' + data.ID;

    var min = [], max = [], avg = [];

    for (var i in data.sources) {
        min.push(data.sources[i].data.reduce((min, p) => p.y < min ? p.y : min, data.sources[i].data[0].y))
        max.push(data.sources[i].data.reduce((max, p) => p.y > max ? p.y : max, data.sources[i].data[0].y))
        avg.push(data.sources[i].data.reduce((total, next) => total + next.y, 0) / data.sources[i].data.length)
    }

    var calcAvg = 0;
    var calcLength = 0;
    for (var i in avg) {
        calcAvg = calcAvg + avg[i] * data.sources[i].data.length;
        calcLength = calcLength + data.sources[i].data.length;
    }
    calcAvg = (calcAvg / calcLength);

    document.getElementById("min" + index).innerHTML = Math.min.apply(Math, min).toFixed(1);
    document.getElementById("avg" + index).innerHTML = calcAvg.toFixed(1);
    document.getElementById("max" + index).innerHTML = Math.max.apply(Math, max).toFixed(1);

    // Alarm stuff
    if (data.Description != "undefined") {
        var description = JSON.parse(data.Description);
        document.getElementById("alarms" + index).innerHTML = alarmCounter;

        if (alarmStatus) {
            document.getElementById("alarmStatus" + index).innerHTML = "Alarm";
        } else {
            document.getElementById("alarmStatus" + index).innerHTML = "OK";
        }

        if (description.crit_high) {
            document.getElementById("crithigh" + index).innerHTML = description.crit_high;
            data.dataset.push(
                {
                    label: 'Critical High',
                    data: [{ t: data.start, y: description.crit_high }, { t: data.end, y: description.crit_high }],
                    borderWidth: 1,
                    borderDash: [5, 5],
                    borderColor: style.getPropertyValue('--danger'),
                    //backgroundColor: '#007bff',
                    fill: false,
                    pointRadius: 0,
                    hoverRadius: 0,
                }
            )
        } else {
            document.getElementById("crithighCol" + index).classList.add("d-none");
        }
        if (description.high) {
            document.getElementById("high" + index).innerHTML = description.high;
            data.dataset.push(
                {
                    label: 'High',
                    data: [{ t: data.start, y: description.high }, { t: data.end, y: description.high }],
                    borderWidth: 1,
                    borderDash: [5, 20],
                    borderColor: style.getPropertyValue('--danger'),
                    //backgroundColor: '#007bff',
                    fill: false,
                    pointRadius: 0,
                    hoverRadius: 0,
                }
            )
        } else {
            document.getElementById("highCol" + index).classList.add("d-none");
        }
        if (description.low) {
            document.getElementById("low" + index).innerHTML = description.low;
            data.dataset.push(
                {
                    label: 'Low',
                    data: [{ t: data.start, y: description.low }, { t: data.end, y: description.low }],
                    borderWidth: 1,
                    borderDash: [5, 20],
                    borderColor: style.getPropertyValue('--primary'),
                    //backgroundColor: '#007bff',
                    fill: false,
                    pointRadius: 0,
                    hoverRadius: 0,
                }
            )
        } else {
            document.getElementById("lowCol" + index).classList.add("d-none");
        }
        if (description.crit_low) {
            document.getElementById("critlow" + index).innerHTML = description.crit_low;
            data.dataset.push(
                {
                    label: 'Critical Low',
                    data: [{ t: data.start, y: description.crit_low }, { t: data.end, y: description.crit_low }],
                    borderWidth: 1,
                    borderDash: [5, 5],
                    borderColor: style.getPropertyValue('--primary'),
                    //backgroundColor: '#007bff',
                    fill: false,
                    pointRadius: 0,
                    hoverRadius: 0,
                }
            )
        } else {
            document.getElementById("critlowCol" + index).classList.add("d-none");
        }
        if (description.duration) {
            document.getElementById("duration" + index).innerHTML = fmtMSS(description.duration) + " minutes";
        } else {
            document.getElementById("duration" + index).innerHTML = fmtMSS(3600) + " minutes";
        }

    } else {
        document.getElementById("alarmNumberRow" + index).classList.add("d-none");
        document.getElementById("alarmConfig" + index).classList.add("d-none");
    }

    var ctx = document.getElementById("myChart" + index);
    var myChart = drawChart(ctx, data);

}

function drawChart(ctx, data) {
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            datasets: data.dataset,
        },
        options: {
            responsive: true,
            legend: {
                display: false
            },
            // elements: {
            //     point: {
            //         radius: 0
            //     }
            // },
            tooltips: {
                position: 'nearest',
                //mode: 'index',
                intersect: false,
                callbacks: {
                    beforeBody: function (tooltipItems, data) {
                        var label = tooltipItems[0].label;
                        return label;
                    },
                    label: function (tooltipItems, data) {
                        var label = tooltipItems.value + '°C';
                        return label;
                    },
                    title: function (tooltipItems, data) {
                        //Return value for title
                        var title = data.datasets[tooltipItems[0].datasetIndex].label || '';
                        return title;
                    },
                }
            },
            scales: {
                xAxes: [{
                    type: 'time',
                    time: {
                        // round: 'hour',                                                                                                                                                                            
                        tooltipFormat: 'YYYY-MM-DD HH:mm',
                        displayFormats: {
                            second: 'HH:mm:ss',
                            minute: 'HH:mm',
                            hour: 'HH',
                            day: 'MMM D'
                        }
                    },
                    display: true,
                    scaleLabel: {
                        display: false,
                        labelString: 'Date'
                    },
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: false,
                        labelString: '°C'
                    },
                    ticks: {
                        // min: min - 1,
                        // max: max + 1,
                        callback: function (value, index, values) {
                            return value + "°C";
                        }
                    }
                }]
            },
        }
    });
    return myChart;
}

function fmtMSS(s){return(s-(s%=60))/60+(9<s?':':':0')+s} // Format seconds to minutes and seconds

function timeConverter(UNIX_timestamp) {
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    if (min < 10) {
        min = "0" + min;
    }
    var sec = a.getSeconds();
    //var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min;
    return time;
}

function drawMarkup(index, sensor) {

    const markup = `
        <div class="row mb-4">
        <div class="col">
        <div class="card" style="width: 100%;">
            <div class="card-header">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                <h1 class="h5" id="chartTitle${index}">Latest Transport</h1>
                <div class="btn-toolbar">
                <div class="btn-group mr-2">
                    <button class="btn btn-sm btn-outline-secondary">Share</button>
                    <button class="btn btn-sm btn-outline-secondary">Export</button>
                </div>
                <button class="btn btn-sm btn-outline-secondary" id="edit${index}" onclick="editButton(${sensor})">Edit</button>
                </div>
            </div>
            </div>

            <div class="card-body">
            <div class="row">
                <div class="col-8">
                <canvas class="my-5" id="myChart${index}" height="100px"></canvas>
                </div>

                <div class="col-4">
                <div class="card">
                    <div class="card-header" id="overviewPanel${index}">
                    Overview
                    </div>
                    <div class="card-body" style="text-align: center;" id = "overview${index}">

                        <div class="row border-bottom mb-3 pb-2" id = "alarmNumberRow${index}">
                            <div class="col">
                                <span>Alarms</span><br><span class="h5" id="alarms${index}"></span>
                            </div>
                            <div class="col">
                            <span>Status</span><br><span class="h5" id="alarmStatus${index}"></span>
                        </div>
                        </div>

                        <div class="row">
                            <div class="col">
                            <span>Minimum</span><br><span class="h5" id="min${index}"></span>
                            </div>
                            <div class="col">
                            <span>Average</span><br><span class="h5" id="avg${index}"></span>
                            </div>
                            <div class="col">
                            <span>Maximum</span><br><span class="h5" id="max${index}"></span>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="card mt-3" id = "alarmConfig${index}">
                    <div class="card-header">
                    Alarm Configuration
                    </div>
                    <div class="card-body" style="text-align: center;">
                        <div class="row border-bottom mb-3 pb-2">
                            <div class="col align-self-end" id="crithighCol${index}">
                            <span>Critical High</span><br><span class="h5" id="crithigh${index}"></span>
                            </div>
                            <div class="col align-self-end" id="highCol${index}">
                            <span>High</span><br><span class="h5" id="high${index}"></span>
                            </div>
                            <div class="col align-self-end" id="lowCol${index}">
                            <span>Low</span><br><span class="h5" id="low${index}"></span>
                            </div>
                            <div class="col align-self-end"id="critlowCol${index}">
                            <span>Critical Low</span><br><span class="h5" id="critlow${index}"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                            <span>Tolerated Duration</span><br><span class="h5" id="duration${index}"></span>
                            </div>
                        </div>
                    </div>
                    
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
        </div>
        `;
    return markup;
}