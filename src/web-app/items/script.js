
const URL = 'http://sofusa-36314.portmap.host:36314';

var request = new XMLHttpRequest()

request.open('GET', URL + "/getItems", true)
request.onload = function () {
    var data = JSON.parse(this.response);

    if (request.status >= 200 && request.status < 400) {     
        for (var i in data) {
            // if (data[i].Location > 0) { //exclude all in area 0
                drawRow(i, data[i]);
            // }
        }

    } else {
        console.log('error')

    }
}

request.send()

var style = getComputedStyle(document.body);
var colorArray = [
    "#259b24",
    "#ff9800",
    "#9c27b0",
    "#7955d8",
    "#cddc39"
];

function drawRow(index, data) {
    
    data.dataset = [];
    data.start = 99999999999999;
    data.end = 0;
    for (var i = 0; i < data.sources.length; i++) {
        
        if (data.sources[i].temperature.length < 1) {
            data.sources.splice(i, 1);
            i = i - 1;
            continue;
        }

        for (var j in data.sources[i].temperature) {
            data.sources[i].temperature[j] = { t: data.sources[i].temperature[j].time * 1000, y: data.sources[i].temperature[j].value };
        }

        data.dataset.push(
            {
                label: 'Area ' + data.sources[i].area,
                data: data.sources[i].temperature,
                borderWidth: 3,
                borderColor: colorArray[data.sources[i].area],
                //backgroundColor: '#007bff',
                fill: false,
                pointRadius: 0,
                hoverRadius: 0,
                lineTension: 0.1
            }
        )
        if (data.sources[i].start < data.start) {
            data.start = data.sources[i].start;
        }
        if (data.sources[i].end === 0) {
            data.end = Date.now();
        } else if (data.sources[i].end > data.end) {
            data.end = data.sources[i].end;
        }



    }
    document.getElementById("chart" + index).innerHTML = drawMarkup(index, data.ID);

    document.getElementById("chartTitle" + index).innerHTML = 'Item: ' + data.ID;
    if (data.dataset[data.dataset.length - 1]) {
        document.getElementById("location" + index).innerHTML = data.dataset[data.dataset.length - 1].label;
    }

    var min = [], max = [], avg = [];
    
    for (var i in data.sources) {
        min.push(data.sources[i].temperature.reduce((min, p) => p.y < min ? p.y : min, data.sources[i].temperature[0].y))
        max.push(data.sources[i].temperature.reduce((max, p) => p.y > max ? p.y : max, data.sources[i].temperature[0].y))
        avg.push(data.sources[i].temperature.reduce((total, next) => total + next.y, 0) / data.sources[i].temperature.length)
    }

    var calcAvg = 0;
    var calcLength = 0;
    for (var i in avg) {
        calcAvg = calcAvg + avg[i] * data.sources[i].temperature.length;
        calcLength = calcLength + data.sources[i].temperature.length;
    }
    calcAvg = (calcAvg / calcLength);

    document.getElementById("min" + index).innerHTML = Math.min.apply(Math, min).toFixed(1);
    document.getElementById("avg" + index).innerHTML = calcAvg.toFixed(1);
    document.getElementById("max" + index).innerHTML = Math.max.apply(Math, max).toFixed(1);

    if (data.desc !== undefined) {
        var description = JSON.parse(data.desc) || 0;
        if (description.crit_high) {
            document.getElementById("crithigh" + index).innerHTML = description.crit_high;
            data.dataset.push(
                {
                    label: 'Critical High',
                    data: [{t: data.start, y: description.crit_high}, {t: data.end, y: description.crit_high}],
                    borderWidth: 1,
                    borderDash: [5,5],
                    borderColor: style.getPropertyValue('--danger'),
                    //backgroundColor: '#007bff',
                    fill: false,
                    pointRadius: 0,
                    hoverRadius: 0,
                }
            )
        } else {
            document.getElementById("crithighCol" + index).classList.add("d-none");
        }
        if (description.high) {
            document.getElementById("high" + index).innerHTML = description.high;
            data.dataset.push(
                {
                    label: 'High',
                    data: [{t: data.start, y: description.high}, {t: data.end, y: description.high}],
                    borderWidth: 1,
                    borderDash: [5,20],
                    borderColor: style.getPropertyValue('--danger'),
                    //backgroundColor: '#007bff',
                    fill: false,
                    pointRadius: 0,
                    hoverRadius: 0,
                }
            )
        } else {
            document.getElementById("highCol" + index).classList.add("d-none");
        }
        if (description.low) {
            document.getElementById("low" + index).innerHTML = description.low;
            data.dataset.push(
                {
                    label: 'Low',
                    data: [{t: data.start, y: description.low}, {t: data.end, y: description.low}],
                    borderWidth: 1,
                    borderDash: [5,20],
                    borderColor: style.getPropertyValue('--primary'),
                    //backgroundColor: '#007bff',
                    fill: false,
                    pointRadius: 0,
                    hoverRadius: 0,
                }
            )
        } else {
            document.getElementById("lowCol" + index).classList.add("d-none");
        }
        if (description.crit_low) {
            document.getElementById("critlow" + index).innerHTML = description.crit_low;
            data.dataset.push(
                {
                    label: 'Critical Low',
                    data: [{t: data.start, y: description.crit_low}, {t: data.end, y: description.crit_low}],
                    borderWidth: 1,
                    borderDash: [5,5],
                    borderColor: style.getPropertyValue('--primary'),
                    //backgroundColor: '#007bff',
                    fill: false,
                    pointRadius: 0,
                    hoverRadius: 0,
                }
            )
        } else {
            document.getElementById("critlowCol" + index).classList.add("d-none");
        }
        if (description.duration) {
            document.getElementById("duration" + index).innerHTML = description.duration + " seconds";
        } else {
            document.getElementById("duration" + index).innerHTML = 3600 + " seconds";
        }

    } else {
        document.getElementById("alarmConfig" + index).classList.add("d-none");
        document.getElementById("alarmCol" + index).classList.add("d-none");
    }

    var ctx = document.getElementById("myChart" + index);
    var myChart = drawChart(ctx, data);

}

function drawChart(ctx, data) {
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            datasets: data.dataset,
        },
        options: {
            responsive: true,
            legend: {
                display: false
            },
            // elements: {
            //     point: {
            //         radius: 0
            //     }
            // },
            tooltips: {
                position: 'nearest',
                //mode: 'index',
                intersect: false,
                callbacks: {
                    beforeBody: function (tooltipItems, data) {
                        var label = tooltipItems[0].label;
                        return label;
                    },
                    label: function (tooltipItems, data) {
                        var label = tooltipItems.value + '°C'
                        return label;
                    },
                    title: function (tooltipItems, data) {
                        //Return value for title
                        var title = data.datasets[tooltipItems[0].datasetIndex].label || '';
                        return title;
                    },
                }

            },
            scales: {
                xAxes: [{
                    type: 'time',
                    time: {
                        // round: 'hour',                                                                                                                                                                            
                        tooltipFormat: 'YYYY-MM-DD HH:mm',
                        displayFormats: {
                            second: 'HH:mm:ss',
                            minute: 'HH:mm',
                            hour: 'HH',
                            day: 'MMM D'
                        }
                    },
                    display: true,
                    scaleLabel: {
                        display: false,
                        labelString: 'Date'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: false,
                        labelString: '°C'
                    },
                    ticks: {
                        // min: min - 1,
                        // max: max + 1,
                        callback: function (value, index, values) {
                            return value + "°C";
                        }
                    }
                }]
            },
        }
    });
    return myChart;
}


function drawMarkup(index, sensor) {

    const markup = `
        <div class="row mb-4">
        <div class="col">
        <div class="card" style="width: 100%;">
            <div class="card-header">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                <h1 class="h5" id="chartTitle${index}">Latest Transport</h1>
                <div class="btn-toolbar">
                <div class="btn-group mr-2">
                    <button class="btn btn-sm btn-outline-secondary">Share</button>
                    <button class="btn btn-sm btn-outline-secondary">Export</button>
                </div>
                <button class="btn btn-sm btn-outline-secondary" id="edit${index}" onclick="editButton(${sensor})">Edit</button>
                </div>
            </div>
            </div>

            <div class="card-body">
            <div class="row">
                <div class="col-8">
                <canvas class="my-4" id="myChart${index}" height="100px"></canvas>
                </div>

                <div class="col-4">
                <div class="card">
                    <div class="card-header" id="overviewPanel${index}">
                    Overview
                    </div>
                    <div class="card-body" style="text-align: center;" id = "overview${index}">
                        <div class="row border-bottom mb-3 pb-2">
                            <div class="col">
                            <span>Current Location</span><br><span class="h5" id="location${index}"></span>
                            </div>
                            <div class="col" id="alarmCol${index}">
                            <span>Alarms</span><br><span class="h5" id="alarms${index}">0</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                            <span>Minimum</span><br><span class="h5" id="min${index}"></span>
                            </div>
                            <div class="col-4">
                            <span>Average</span><br><span class="h5" id="avg${index}"></span>
                            </div>
                            <div class="col-4">
                            <span>Maximum</span><br><span class="h5" id="max${index}"></span>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="card mt-3" id = "alarmConfig${index}">
                    <div class="card-header">
                    Alarm Configuration
                    </div>
                    <div class="card-body" style="text-align: center;">
                        <div class="row border-bottom mb-3 pb-2">
                            <div class="col" id="crithighCol${index}">
                            <span>Critical High</span><br><span class="h5" id="crithigh${index}"></span>
                            </div>
                            <div class="col" id="highCol${index}">
                            <span>High</span><br><span class="h5" id="high${index}"></span>
                            </div>
                            <div class="col" id="lowCol${index}">
                            <span>Low</span><br><span class="h5" id="low${index}"></span>
                            </div>
                            <div class="col"id="critlowCol${index}">
                            <span>Critical Low</span><br><span class="h5" id="critlow${index}"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                            <span>Duration</span><br><span class="h5" id="duration${index}"></span>
                            </div>
                        </div>
                    </div>
                    
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
        </div>
        `;
    return markup;
}